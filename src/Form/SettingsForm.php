<?php

namespace Drupal\dg_measurement_protocol\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure dg_measurement_protocol settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dg_measurement_protocol_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tracker_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracker ID'),
      '#size' => 15,
      '#default_value' => $this->config('dg_measurement_protocol.settings')
        ->get('tracker_id'),
    ];
    $form['tracking_connected_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Tracking connected users (Extranet/Intranet website for example)'),
      '#default_value' => $this->config('dg_measurement_protocol.settings')
        ->get('tracking_connected_users') ?? FALSE,
    ];
    $form['language_setting'] = [
      '#type' => 'radios',
      '#title' => $this->t('Language setting'),
      '#default_value' => $this->config('dg_measurement_protocol.settings')
        ->get('language_setting') ?? 'browser',
      '#options' => [
        'browser' => $this->t('Browser langcode'),
        'drupal' => $this->t('Drupal langcode'),
      ],
    ];
    $form['ip_variable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('$_SERVER IP variable'),
      '#size' => 15,
      '#default_value' => $this->config('dg_measurement_protocol.settings')
        ->get('ip_variable') ?? 'HTTP_X_REAL_IP',
    ];
    $form['ip_variable_index'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X_FORWARDED_FOR IP index'),
      '#size' => 15,
      '#default_value' => '0',
      '#description' => $this->t("If you select X_FORWARDED_FOR, you've to inform this value. First item is 0"),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dg_measurement_protocol.settings')
      ->set('tracker_id', $form_state->getValue('tracker_id'))
      ->set('tracking_connected_users', $form_state->getValue('tracking_connected_users'))
      ->set('language_setting', $form_state->getValue('language_setting'))
      ->set('ip_variable', $form_state->getValue('ip_variable'))
      ->set('ip_variable_index', $form_state->getValue('ip_variable_index'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dg_measurement_protocol.settings'];
  }

}
