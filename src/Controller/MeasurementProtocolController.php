<?php

namespace Drupal\dg_measurement_protocol\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for dg_measurement_protocol routes.
 */
class MeasurementProtocolController extends ControllerBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A loggerFactory instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs controller.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The loggerFactory.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $loggerFactory) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $loggerFactory->get('dg_measurement_protocol');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('logger.factory')
    );
  }

  /**
   * Builds the response.
   */
  public function send() {
    $tracker_id = $this->configFactory->get('dg_measurement_protocol.settings')
      ->get('tracker_id');
    $language_setting = $this->configFactory->get('dg_measurement_protocol.settings')
      ->get('language_setting');
    $tracking_connected_users = $this->configFactory->get('dg_measurement_protocol.settings')
      ->get('tracking_connected_users');

    if (!$tracker_id) {
      return new JsonResponse([]);
    }

    if (!isset($_POST['t'], $_POST['u'], $_POST['r'], $_POST['tkn'])) {
      return new JsonResponse([]);
    }

    $allCrawlersPattern = "(Googlebot\\/|Googlebot-Mobile|Googlebot-Image|Googlebot-News|Googlebot-Video|AdsBot-Google([^-]|$)|AdsBot-Google-Mobile|Feedfetcher-Google|Mediapartners-Google|Mediapartners \\(Googlebot\\)|APIs-Google|bingbot|Slurp|[wW]get|LinkedInBot|Python-urllib|python-requests|aiohttp|httpx|libwww-perl|httpunit|nutch|Go-http-client|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|BIGLOTRON|Teoma|convera|seekbot|Gigabot|Gigablast|exabot|ia_archiver|GingerCrawler|webmon |HTTrack|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|findlink|msrbot|panscient|yacybot|AISearchBot|ips-agent|tagoobot|MJ12bot|woriobot|yanga|buzzbot|mlbot|YandexBot|YandexImages|YandexAccessibilityBot|YandexMobileBot|YandexMetrika|YandexTurbo|YandexImageResizer|YandexVideo|YandexAdNet|YandexBlogs|YandexCalendar|YandexDirect|YandexFavicons|YaDirectFetcher|YandexForDomain|YandexMarket|YandexMedia|YandexMobileScreenShotBot|YandexNews|YandexOntoDB|YandexPagechecker|YandexPartner|YandexRCA|YandexSearchShop|YandexSitelinks|YandexSpravBot|YandexTracker|YandexVertis|YandexVerticals|YandexWebmaster|YandexScreenshotBot|purebot|Linguee Bot|CyberPatrol|voilabot|Baiduspider|citeseerxbot|spbot|twengabot|postrank|TurnitinBot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|Ahrefs(Bot|SiteAudit)|fuelbot|CrunchBot|IndeedBot|mappydata|woobot|ZoominfoBot|PrivacyAwareBot|Multiviewbot|SWIMGBot|Grobbot|eright|Apercite|semanticbot|Aboundex|domaincrawler|wbsearchbot|summify|CCBot|edisterbot|seznambot|ec2linkfinder|gslfbot|aiHitBot|intelium_bot|facebookexternalhit|Yeti|RetrevoPageAnalyzer|lb-spider|Sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|OrangeBot\\/|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|S[eE][mM]rushBot|yoozBot|lipperhey|Y!J|Domain Re-Animator Bot|AddThis|Screaming Frog SEO Spider|MetaURI|Scrapy|Livelap[bB]ot|OpenHoseBot|CapsuleChecker|collection@infegy.com|IstellaBot|DeuSu\\/|betaBot|Cliqzbot\\/|MojeekBot\\/|netEstate NE Crawler|SafeSearch microdata crawler|Gluten Free Crawler\\/|Sonic|Sysomos|Trove|deadlinkchecker|Slack-ImgProxy|Embedly|RankActiveLinkBot|iskanie|SafeDNSBot|SkypeUriPreview|Veoozbot|Slackbot|redditbot|datagnionbot|Google-Adwords-Instant|adbeat_bot|WhatsApp|contxbot|pinterest.com.bot|electricmonk|GarlikCrawler|BingPreview\\/|vebidoobot|FemtosearchBot|Yahoo Link Preview|MetaJobBot|DomainStatsBot|mindUpBot|Daum\\/|Jugendschutzprogramm-Crawler|Xenu Link Sleuth|Pcore-HTTP|moatbot|KosmioBot|pingdom|AppInsights|PhantomJS|Gowikibot|PiplBot|Discordbot|TelegramBot|Jetslide|newsharecounts|James BOT|Bark[rR]owler|TinEye|SocialRankIOBot|trendictionbot|Ocarinabot|epicbot|Primalbot|DuckDuckGo-Favicons-Bot|GnowitNewsbot|Leikibot|LinkArchiver|YaK\\/|PaperLiBot|Digg Deeper|dcrawl|Snacktory|AndersPinkBot|Fyrebot|EveryoneSocialBot|Mediatoolkitbot|Luminator-robots|ExtLinksBot|SurveyBot|NING\\/|okhttp|Nuzzel|omgili|PocketParser|YisouSpider|um-LN|ToutiaoSpider|MuckRack|Jamie's Spider|AHC\\/|NetcraftSurveyAgent|Laserlikebot|^Apache-HttpClient|AppEngine-Google|Jetty|Upflow|Thinklab|Traackr.com|Twurly|Mastodon|http_get|DnyzBot|botify|007ac9 Crawler|BehloolBot|BrandVerity|check_http|BDCbot|ZumBot|EZID|ICC-Crawler|ArchiveBot|^LCC |filterdb.iss.net\\/crawler|BLP_bbot|BomboraBot|Buck\\/|Companybook-Crawler|Genieo|magpie-crawler|MeltwaterNews|Moreover|newspaper\\/|ScoutJet|(^| )sentry\\/|StorygizeBot|UptimeRobot|OutclicksBot|seoscanners|Hatena|Google Web Preview|MauiBot|AlphaBot|SBL-BOT|IAS crawler|adscanner|Netvibes|acapbot|Baidu-YunGuanCe|bitlybot|blogmuraBot|Bot.AraTurka.com|bot-pge.chlooe.com|BoxcarBot|BTWebClient|ContextAd Bot|Digincore bot|Disqus|Feedly|Fetch\\/|Fever|Flamingo_SearchEngine|FlipboardProxy|g2reader-bot|G2 Web Services|imrbot|K7MLWCBot|Kemvibot|Landau-Media-Spider|linkapediabot|vkShare|Siteimprove.com|BLEXBot\\/|DareBoost|ZuperlistBot\\/|Miniflux\\/|Feedspot|Diffbot\\/|SEOkicks|tracemyfile|Nimbostratus-Bot|zgrab|PR-CY.RU|AdsTxtCrawler|Datafeedwatch|Zabbix|TangibleeBot|google-xrawler|axios|Amazon CloudFront|Pulsepoint|CloudFlare-AlwaysOnline|Google-Structured-Data-Testing-Tool|WordupInfoSearch|WebDataStats|HttpUrlConnection|Seekport Crawler|ZoomBot|VelenPublicWebCrawler|MoodleBot|jpg-newsbot|outbrain|W3C_Validator|Validator\\.nu|W3C-checklink|W3C-mobileOK|W3C_I18n-Checker|FeedValidator|W3C_CSS_Validator|W3C_Unicorn|Google-PhysicalWeb|Blackboard|ICBot\\/|BazQux|Twingly|Rivva|Experibot|awesomecrawler|Dataprovider.com|GroupHigh\\/|theoldreader.com|AnyEvent|Uptimebot\\.org|Nmap Scripting Engine|2ip.ru|Clickagy|Caliperbot|MBCrawler|online-webceo-bot|B2B Bot|AddSearchBot|Google Favicon|HubSpot|Chrome-Lighthouse|HeadlessChrome|CheckMarkNetwork\\/|www\\.uptime\\.com|Streamline3Bot\\/|serpstatbot\\/|MixnodeCache\\/|^curl|SimpleScraper|RSSingBot|Jooblebot|fedoraplanet|Friendica|NextCloud|Tiny Tiny RSS|RegionStuttgartBot|Bytespider|Datanyze|Google-Site-Verification|TrendsmapResolver|tweetedtimes|NTENTbot|Gwene|SimplePie|SearchAtlas|Superfeedr|feedbot|UT-Dorkbot|Amazonbot|SerendeputyBot|Eyeotabot|officestorebot|Neticle Crawler|SurdotlyBot|LinkisBot|AwarioSmartBot|AwarioRssBot|RyteBot|FreeWebMonitoring SiteChecker|AspiegelBot|NAVER Blog Rssbot|zenback bot|SentiBot|Domains Project\\/|Pandalytics|VKRobot|bidswitchbot|tigerbot|NIXStatsbot|Atom Feed Robot|Curebot|PagePeeker\\/|Vigil\\/|rssbot\\/|startmebot\\/|JobboerseBot|seewithkids|NINJA bot|Cutbot|BublupBot|BrandONbot|RidderBot|Taboolabot|Dubbotbot|FindITAnswersbot|infoobot|Refindbot|BlogTraffic\\/\\d\\.\\d+ Feed-Fetcher|SeobilityBot|Cincraw|Dragonbot|VoluumDSP-content-bot|FreshRSS|BitBot|^PHP-Curl-Class|Google-Certificates-Bridge)";
    if (preg_match($allCrawlersPattern, $_SERVER['HTTP_USER_AGENT']) ||
      substr($_SERVER['HTTP_USER_AGENT'], 0, 8) !== 'Mozilla/') {
      return new JsonResponse([]);
    }

    $token_rebuild = base64_encode($_POST['t'] . $_POST['u'] . $_POST['r']);
    if ($token_rebuild !== $_POST['tkn']) {
      return new JsonResponse([]);
    }

    $user = \Drupal::currentUser();
    if ($user->id() !== 0 && !$tracking_connected_users) {
      return new JsonResponse([]);
    }

    $url = 'https://www.google-analytics.com/collect';
    $post_data = $this->buildPostData($tracker_id, $language_setting);
    if (empty($post_data)) {
      return new JsonResponse([]);
    }

    try {
      $this->httpClient->{'POST'}($url, ['form_params' => $post_data]);
      $response = new JsonResponse(['msg' => 'success']);
      $response->headers->setCookie(new Cookie('anonymous_uid', $post_data['cid'], time() + 365 * 86400, '/', NULL, TRUE, TRUE));
      return $response;
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->error('Failed to complete Measurement Protocol request : %post', [
        '%post' => http_build_query($post_data),
      ]);
      $this->loggerFactory->error('Exception : %exception', [
        '%exception' => $exception->getMessage(),
      ]);
    }
    return new JsonResponse(['msg' => 'exception']);
  }

  /**
   * Prepare post data.
   *
   * @param string $tracker_id
   *   Tracker ID.
   * @param string $language_setting
   *   Language setting.
   *
   * @return array
   */
  private function buildPostData(string $tracker_id, string $language_setting): array {
    $hit_type = 'pageview';
    $is_new_session = FALSE;

    // Anonymous Client ID.
    if ((isset($_COOKIE['anonymous_uid'])) && (strlen($_COOKIE['anonymous_uid']) == 36)) {
      // Get Anonymous Client ID.
      $cid = $_COOKIE['anonymous_uid'];
    }
    else {
      // Generate Anonymous Client ID.
      $cid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
      setcookie('anonymous_uid', $cid, time() + 365 * 86400, '/', NULL, TRUE, TRUE);
      $is_new_session = TRUE;
    }

    // Anonymise IP.
    $uip = $this->getIp();
    if (empty($uip)) {
      return [];
    }

    if ($language_setting === 'browser') {
      // Browser language.
      $xx = explode(',', isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : 'fr');
      $ul = $xx[0];
    }
    else {
      $ul = strtolower(\Drupal::languageManager()
        ->getCurrentLanguage()
        ->getId());
    }

    // Set data array.
    $data = [
      'v' => 1,
      'tid' => $tracker_id,
      'ds' => 'web',
      'uip' => $uip,
      't' => $hit_type,
      'dt' => $_POST['t'],
      'dl' => $this->cleanUrlQuery($_POST['u']),
      'ua' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
      'cid' => $cid,
      'dr' => isset($_POST['r']) ? $_POST['r'] : '',
      'ul' => $ul,
      'cd19' => 'Anonyme',
    ];

    // Referrer
    if (parse_url($_POST['u'], PHP_URL_HOST) === parse_url($_POST['r'], PHP_URL_HOST)
      && !$is_new_session) {
      unset($data['dr']);
    }

    // Handling utm_*
    if (isset($_GET['utm_campaign'])) {
      $data['cn'] = $_GET['utm_campaign'];
    }
    if (isset($_GET['utm_medium'])) {
      $data['cm'] = $_GET['utm_medium'];
    }
    if (isset($_GET['utm_source'])) {
      $data['cs'] = $_GET['utm_source'];
    }

    return $data;
  }

  private function getIp() {
    $ip = '';
    $ip_variable = $this->configFactory->get('dg_measurement_protocol.settings')
      ->get('ip_variable');
    $ip_variable_index = $this->configFactory->get('dg_measurement_protocol.settings')
      ->get('ip_variable_index');

    switch ($ip_variable) {
      case 'HTTP_X_REAL_IP':
      case 'REMOTE_ADDR':
        $ip = $_SERVER[$ip_variable];
        break;
      case 'HTTP_X_FORWARDED_FOR':
        $list_ip = explode(',', $_SERVER[$ip_variable]);
        if (isset($list_ip[$ip_variable_index])) {
          $ip = trim($list_ip[$ip_variable_index]);
        }
        break;
      default:
        $ip = '';
        break;
    }

    $regex_ipv4 = '/^(25[0–5]|2[0–4][0–9]|[01]?[0–9][0–9]?).(25[0–5]|2[0–4][0–9]|[01]?[0–9][0–9]?).(25[0–5]|2[0–4][0–9]|[01]?[0–9][0–9]?).(25[0–5]|2[0–4][0–9]|[01]?[0–9][0–9]?)$/';
    if (!empty($ip)) {
      if (preg_match($regex_ipv4, $ip)) {
        $ip_elements = explode('.', $ip);
        $ip = $ip_elements[0] . '.' . $ip_elements[1] . '.0.0';
      }
      else {
        $lastPos = 0;
        while (($lastPos = strpos($ip, ':', $lastPos)) !== FALSE) {
          $positions[] = $lastPos;
          $lastPos = $lastPos + strlen(':');
        }

        if (isset($positions[2])) {
          $ip = substr($ip, 0, $positions[2]) . '::1';
        }
      }
    }

    return $ip;
  }

  /**
   * Remove gclid query parameter.
   *
   * @param string $url
   *   URl.
   *
   * @return string
   *   Url rebuild.
   */
  private function cleanUrlQuery(string $url): string {
    $url_parsed = parse_url($url);
    if (isset($url_parsed['query']) && !empty($url_parsed['query'])) {
      parse_str($url_parsed['query'], $params);
      if (isset($params['gclid'])) {
        unset($params['gclid']);
      }
      $url_parsed['query'] = http_build_query($params);
    }


    return (isset($url_parsed['scheme']) ? "{$url_parsed['scheme']}:" : '') .
      ((isset($url_parsed['user']) || isset($url_parsed['host'])) ? '//' : '') .
      (isset($url_parsed['user']) ? "{$url_parsed['user']}" : '') .
      (isset($url_parsed['pass']) ? ":{$url_parsed['pass']}" : '') .
      (isset($url_parsed['user']) ? '@' : '') .
      (isset($url_parsed['host']) ? "{$url_parsed['host']}" : '') .
      (isset($url_parsed['port']) ? ":{$url_parsed['port']}" : '') .
      (isset($url_parsed['path']) ? "{$url_parsed['path']}" : '') .
      ((isset($url_parsed['query']) && !empty($url_parsed['query'])) ? "?{$url_parsed['query']}" : '') .
      (isset($url_parsed['fragment']) ? "#{$url_parsed['fragment']}" : '');
  }

}
