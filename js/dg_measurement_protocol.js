(function ($, drupalSettings) {
  'use strict';

  $.ajax({
    type: 'POST',
    url: drupalSettings.dg_measurement_protocol.langcode_prefix + '/ajax/measurement-protocol/send',
    data: {
      't': document.title,
      'u': window.location.href,
      'r': document.referrer,
      'tkn': window.btoa(unescape(encodeURIComponent(document.title + window.location.href + document.referrer))),
    },
    dataType: 'json',
    error: function
        (response) {
      console.info('error ajax measurement protocol');
    },
    success: function
        (response) {
    }
  });
}(jQuery, drupalSettings));